#https://www.800txt.net/ 800小说网
import sys
import aiohttp
import requests
from aiohttp import ClientSession
from bs4 import BeautifulSoup
from vender.book import Book
from Lib import asyncio
from app.db import BookModel,ChapterModel
from Lib.urllib.parse import quote
from tqdm import tqdm
class Txt800:
    title = '800小说网'
    url = 'https://www.800txt.net'
    async def find(self,str):                           
        try:
            sys.setrecursionlimit(1000000)
            async with aiohttp.ClientSession() as session:
                url = self.url+'/modules/article/search.php?searchkey='+ quote(str.encode('gbk'))
                async with session.get(url,timeout=20) as response:
                    html = await response.text('gb2312','ignore')                
                    return self.parseFind(html)
        except BaseException:
            return []            

        
    # 解析搜索结果 
    # 结果目前有两种
    # 1多个符合名称的书籍 展示的是列表页
    # 2只有一个符合名称的书籍 直接展示的是书籍目录页
    def parseFind(self,html):
        obj = BeautifulSoup(html,'lxml')
        res = obj.select('.novelslistss li') #列表页解析       
        result = []
        if res:                        
            for item in res:             
                name = item.select('span')[1].string
                author = item.select('span')[3].string
                update_time = item.select('span')[4].string
                status = item.select('span')[5].string
                url = item.select('span')[1].select_one('a').attrs['href']
                result.append(Book(name,author,update_time,status,url,self.title,self.__class__.__name__))
        else:
            name = obj.select_one('meta[property="og:novel:book_name"]').get('content')
            author = obj.select_one('meta[property="og:novel:author"]').get('content')
            update_time = obj.select_one('meta[property="og:novel:update_time"]').get('content')
            status = obj.select_one('meta[property="og:novel:status"]').get('content')
            url = obj.select_one('meta[property="og:url"]').get('content')
            result.append(Book(name,author,update_time,status,url,self.title,self.__class__.__name__))
        return result        

    # 下载书籍
    def down(self,book):        

        b1 = BookModel.create(name=book.name,author=book.author,update_time=book.update_time,status=book.status,url=book.url,source=book.source,target=book.target,state=0)

        # 尝试下载目录次数
        getDirCount = 3
        while getDirCount>0 and len(book.chapter)==0:
            print('获取目录')
            getDirCount-=1
            self.getDirectory(book)        
            print('获取目录成功')
            if len(book.chapter)>0:
                b1.state=1
                b1.save()
                for item in book.chapter:
                    ChapterModel.create(book=b1,url=item['url'],title=item['title'],status=0)

        if b1.state==0:
            print('获取目录失败')
            return 

        if b1.state==3:
            print('下载完成')

        
        print('开始下载章节')
        b1.state=2
        b1.save()
        self.pbar = tqdm(total= len(book.chapter) ) 
        while True:
            tempChapters = []
            for chapter in b1.chapters.where(ChapterModel.status==0): #.paginate(1,100)
                tempChapters.append(chapter)
            if len(tempChapters)==0:
                break
            #print('开始下载任务 任务数量',len(tempChapters))
            try:
                contentList = asyncio.run(self.DownBook(tempChapters))
                #print('已完成当前批次任务')               
            except BaseException:                
                print('任务异常')
            tempChapters.clear()
        b1.state = 3
        b1.save()    
        self.pbar.close()    
        print('下载完成')

    # 获取书的目录
    def getDirectory(self,book):        
        html = self.httpGet(book.url)
        obj = BeautifulSoup(html,'lxml')
        res = obj.select("#list dd a")        
        for item in res:
            book.chapter.append( {
                'title': item.text ,
                'url':book.url+item.attrs['href']
            })
    
    # 获取单章内容
    async def DownBook(self,chapters):                  
        tasks = []        
        for chapter in chapters:
            task = asyncio.create_task(self.getOnce(chapter))
            tasks.append(task)
        res = await asyncio.gather(*tasks)
        return res

    async def getOnce(self,chapter):
        try:        
            async with aiohttp.ClientSession() as session:
                async with session.get(chapter.url,timeout=20) as response:
                    html = await response.text('gb2312','ignore')
                    obj = BeautifulSoup(html,'lxml')
                    #return obj.select_one('title').text
                    content = obj.select_one("#content")
                    chapter.content = content.text.replace('一秒记住【800♂小÷说→网 WwW.800TXT.NET】，精彩小说无弹窗免费阅读！','')
                    chapter.status = 1
                    chapter.save()
                    #print(chapter.title,'下载完成')
                    self.pbar.update(1)
        except BaseException:
            return ''

    def httpGet(self,url):
        response = requests.get(url,timeout=20)
        response.encoding = 'GB2312'
        return response.text
        