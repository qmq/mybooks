import sys

from prettytable import PrettyTable

from Lib import asyncio
from vender.book import Book
from vender.txt800 import Txt800
from vender.xs52 import Xs52

from .db import *


class App:
    books = []

    def init(self):
        self.menu()

    def menu(self):
        table = PrettyTable(['指令', '菜单'])
        table.add_row(['s', '搜索书箱'])
        table.add_row(['d', '我的下载'])
        print(table)

        self.cmd()

    def cmd(self):
        cmd = input('请输入指令:')

        if cmd == 's':
            name = input('请输入搜索的书籍名称/或q退出:')
            if name == 'q':
                pass
            else:
                asyncio.run(self.find(name))
                if len(self.books):
                    d_tip: bool = True
                    while d_tip:
                        index = input('请输入序号下载对应的书籍:')
                        if index == 'q':
                            d_tip = False
                        else:
                            if index:
                                i = int(index)
                                if i <= len(self.books):
                                    self.down(self.books[i - 1])
                                    self.showFindBooks()

        if cmd == 'd':
            pass

        self.menu()

    # 查找书
    async def find(self, name):
        self.books.clear()

        # 查找     
        tasks = []
        task = asyncio.create_task(Txt800().find(name))
        task1 = asyncio.create_task(Xs52().find(name))
        tasks.append(task)
        tasks.append(task1)
        # try:
        res = await asyncio.gather(*tasks)

        books = sum(res, [])
        if len(books):
            self.books = books
            self.showFindBooks()
        else:
            print('未找到相关名称书籍')

        # except BaseException as err:
        # print(err)

    def showFindBooks(self):
        table = PrettyTable(['序号', '书名', '作者', '来源', '更新时间', '状态'])
        for index, book in enumerate(self.books):
            book.chapter.clear()
            table.add_row(book.toArray(index + 1))
        print(table)

    # 下载书
    def down(book):
        globals()[book.target]().down(book)
        return
