from peewee import *

db = SqliteDatabase('mydown.db3')
db.connect()


class BookModel(Model):
    name = CharField(255)
    author = CharField()
    update_time = CharField()
    status = CharField()
    url = CharField()
    source = CharField()
    target = CharField()
    # 0创建了任务 1获取到了目录 2下载中 3下载完成
    state = IntegerField()

    class Meta:
        database = db


class ChapterModel(Model):
    url = CharField(255)
    title = CharField(verbose_name='章节名', null=True)
    content = TextField(null=True)
    book = ForeignKeyField(BookModel, backref='chapters')
    status = IntegerField()

    class Meta:
        database = db


db.create_tables([BookModel, ChapterModel])
