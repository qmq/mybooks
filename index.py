import sys
from app.app import App
from PyQt5.QtWidgets import QApplication,QMainWindow
from view.Ui_main import Ui_MainWindow
class MyWindow(QMainWindow,Ui_MainWindow):
    def __init__(self,parent=None):
        super(MyWindow,self).__init__(parent)
        self.setupUi(self)
    
        

if __name__ == "__main__":
    #app = App()
    #app.init()
    app = QApplication(sys.argv)
    myWin = MyWindow()        
    myWin.show()
    sys.exit(app.exec_())




