class Book():
    """ 书籍类 
    
    Attributes:
        name:书籍名称
        author:作者
        status:状态 连载中 完结
        update_time:更新时间
        source:来源
        url:来源地址
        target:来源类
        state:状态  0:已获取书籍信息,未获取到章节目录  1:获取到章节目录 2:已完成下载
        chapters:章节列表
    """

    chapters = []
    class Chapter():
        """ 章节类

        Attributes:
            title:章节名
            url:章节地址
            content:章节内容
            status:状态 0:未下载 1:已下载        
        """
        def __init__(self, title,url,content=None,status=0,book=None):
            self.title = title
            self.url = url
            self.content = content
            self.status = status  
            self.book = book

    

    def __init__(self,name,author,status,update_time=None,source=None,url=None,target=None,state=0):
        self.name = name
        self.author = author
        self.status = status
        self.update_time = update_time
        self.source = source
        self.url = url
        self.target = target
        self.state = state
        
    # 给书籍新增一个章节
    def addChapter(self,title,url,content=None,status=0):
        chapter = Book.Chapter(title,url,content,status,self)
        self.chapters.append(chapter)

