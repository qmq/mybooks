from PyQt5.QtWidgets import QHBoxLayout,QLabel,QVBoxLayout,QProgressBar,QPushButton,QWidget
from PyQt5.QtCore import pyqtSignal,QUrl,QObject
from PyQt5.QtNetwork import QNetworkAccessManager,QNetworkRequest
from app2.types import Book
# ListItem布局实例
class ListItem(QWidget):
    #下载器状态 0:暂停 1:开始
    downStatus=0  
    
    def __init__(self, book:Book, parent=None ):
        super().__init__(parent=parent)
        self.book = book

        self.init()
    def init(self):
        self.taskManger=ListItem.Thread(self.book)
        self.taskManger.started.connect(self.startTask)
        self.taskManger.stoped.connect(self.stopTask)
        self.taskManger.finished.connect(self.finished)
        self.taskManger.changed.connect(self.changeProgressBar)
        

        layout = QHBoxLayout()

        left = QVBoxLayout()

        label1 = QLabel() 
        label1.setText(self.book.name)
        left.addWidget(label1)


        bottom = QHBoxLayout()
        label2 = QLabel()
        label2.setText("总共"+ str( len(self.book.chapters)) +"章")
        bottom.addWidget(label2)

        bar = QProgressBar()        
        bar.setSizeIncrement(50,30)   
        # 设置进度条的最小值与最大值
        bar.setRange(0,5)                  
        self.bar = bar  
        self.step = 0      
        bottom.addWidget(bar)
                
        
        left.addLayout(bottom)        
        layout.addLayout(left,20)

        # 0 已获取书籍信息,未获取到章节目录
        # 1 获取到章节目录,可以开始下载
        # 2 下载完成       
        # 右侧按钮
        rightBtn = QPushButton('下载')
        if self.book.state==2:
            rightBtn.setText('导出')
        rightBtn.clicked.connect(self.btnClicked)
        self.rightBtn = rightBtn        
        layout.addWidget(rightBtn,1)
        self.setLayout(layout)
        return self


    def changeProgressBar(self):
        self.step+=1
        self.bar.setValue(self.step)
        if self.step>=5:
            #下载完成
            self.downStatus = 0            
            self.book.state=2
            self.rightBtn.setText('导出')        
            self.taskManger.quit()
            

    def btnClicked(self):        
        if self.book.state==2:#已下载完成            
            self.downStatus = 0        
            print('导出书籍')
        elif self.downStatus==0: #当前为暂停状态 设置开启下载任务 显示暂停
            self.downStatus = 1
            self.rightBtn.setText('暂停')
        elif self.downStatus==1: #当前为下载中状态 设置任务为暂停 显示下载
            self.downStatus = 0
            self.rightBtn.setText('下载')
        
        if self.downStatus==0: #暂停任务
            self.taskManger.stop()
        elif self.downStatus==1: #开启任务
            self.taskManger.start()

    def stopTask(self):
        print('暂停一个线程任务')
    def startTask(self):
        print('开启一个线程任务')
    def finished(self):
        print('完成一个线程任务')

    #QNetworkAccessManager 是一个异步请求管理器，这里就不需要再用异步任务
    class Thread(QObject): 
        started = pyqtSignal() #启动任务
        finished = pyqtSignal() #完成任务
        stoped = pyqtSignal() #暂停任务
        changed = pyqtSignal() #任务进度
        def __init__(self,book:Book,parent=None):
                super().__init__(parent=parent)
                self.book = book
        
        def run(self):
            self.started.emit()
            if self.book.state==0:            
                self._downDirectory()
            elif self.book.state == 1:
                self._downChapter()
            else:                
                pass
        def _downDirectory(self):
            print("下载目录")
            http = QNetworkAccessManager(self)
            self.http = http
            
            http.finished.connect(self._parseDirectory)
            http.get(QNetworkRequest(QUrl(self.book.url)))
            #http.get(QNetworkRequest(QUrl(self.book.url)))
            #http.get(QNetworkRequest(QUrl(self.book.url)))
            
        
        def _parseDirectory(self,reply):
            er = reply.error()
            string = reply.readAll().data()            
            # print(self.http)
            # print(er)
            print(string)
        def _downChapter(self):
            print("下载章节")
            self.changed.emit()
            pass
        def stop(self):
            self.stoped.emit()
            pass
        def start(self):
            self.run()
            pass
        def quit(self):
            self.finished.emit()
            pass
