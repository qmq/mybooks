#窗口布局
from PyQt5.QtWidgets import QWidget,QVBoxLayout,QHBoxLayout,QLineEdit,QPushButton,QListWidgetItem,QMainWindow,QListWidget
from PyQt5 import Qt
from app2.types import Book
from app2.widgets import ListItem
# 主窗口程序
class MyWin(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.init()

    def init(self):
        self.resize(800,600)


        centerWin = QWidget()
        #centerWin.setStyleSheet("background-color:rgba(0,0,0,0.1)")
        layout = QVBoxLayout()                
        centerWin.setLayout(layout)
        self.setCentralWidget(centerWin)
        
        topSearch = TopSearch()        
        layout.addLayout(topSearch,1)

        
        centerBox = CenterBox()
        layout.addWidget(centerBox,20)
  

# 主窗口顶部内容
class TopSearch(QHBoxLayout):
    def __init__(self):
        super().__init__()
        self.init()

    def init(self):
        text = QLineEdit()
        text.setPlaceholderText("请输入书名或作者名")        
        text.setFont(Qt.QFont(None,26))

        btn = QPushButton('搜索')   
        btn.setMinimumHeight(50)
        
        self.addWidget(text)        
        self.addWidget(btn)



# 窗口中间正文内容
class CenterBox(QListWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.init()

    def init(self):

        item = QListWidgetItem()
        item.setSizeHint( Qt.QSize(0,80) )        
        self.addItem(item)

        book = Book('龙蛇演义','梦入神机','已完结','2019-10-10 12:00:00','小说100','https://www.baidu.com','Xs100',0)
        #ch1 = book.addChapter('第一章 飞入成龙','http://www.baidu.com')
        #ch2 = book.addChapter('第二章 龙蛇起陆 万','http://www.baidu.com/2')

        self.setItemWidget(item, ListItem(book) )                
        

      
  